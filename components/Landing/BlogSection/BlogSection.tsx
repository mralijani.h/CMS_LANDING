import { FC } from "react";
import BlogSectionProps from "./../../../logic/inteface/BlogSection";
import style from "../../../styles/BlogSection.module.css";
import Button from "./../../common/Button/Button";
import Carousel from "react-multi-carousel";
import img from "../../../assets/images/download.jpg";
import BlogCard from "./../../common/Cards/BlogCard/BlogCard";
const BlogSection: FC<BlogSectionProps> = ({}: BlogSectionProps) => {
  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 4,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 600 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 600, min: 0 },
      items: 1,
    },
  };

  const image = [
    {
      imageUrl: img.src,
      title: "اتحادیه، رونق بخش صنایع خوراک دام،طیور و آبزیان استان سمنان",
      description:
        "مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان برتر و تراز اول کشوردر زمینه تولید انواع خوراک دام،طیور و آبزیان مجموعه ای از بزرگتر",
    },
    {
      imageUrl: img.src,
      title: "اتحادیه، رونق بخش صنایع خوراک دام،طیور و آبزیان استان سمنان",
      description:
        "مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان برتر و تراز اول کشوردر زمینه تولید انواع خوراک دام،طیور و آبزیان مجموعه ای از بزرگتر",
    },
    {
      imageUrl: img.src,
      title: "اتحادیه، رونق بخش صنایع خوراک دام،طیور و آبزیان استان سمنان",
      description:
        "مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان برتر و تراز اول کشوردر زمینه تولید انواع خوراک دام،طیور و آبزیان مجموعه ای از بزرگتر",
    },
    {
      imageUrl: img.src,
      title: "اتحادیه، رونق بخش صنایع خوراک دام،طیور و آبزیان استان سمنان",
      description:
        "مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان برتر و تراز اول کشوردر زمینه تولید انواع خوراک دام،طیور و آبزیان مجموعه ای از بزرگتر",
    },
    {
      imageUrl: img.src,
      title: "اتحادیه، رونق بخش صنایع خوراک دام،طیور و آبزیان استان سمنان",
      description:
        "مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان برتر و تراز اول کشوردر زمینه تولید انواع خوراک دام،طیور و آبزیان مجموعه ای از بزرگتر",
    },
    {
      imageUrl: img.src,
      title: "اتحادیه، رونق بخش صنایع خوراک دام،طیور و آبزیان استان سمنان",
      description:
        "مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان برتر و تراز اول کشوردر زمینه تولید انواع خوراک دام،طیور و آبزیان مجموعه ای از بزرگتر",
    },
    {
      imageUrl: img.src,
      title: "اتحادیه، رونق بخش صنایع خوراک دام،طیور و آبزیان استان سمنان",
      description:
        "مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان برتر و تراز اول کشوردر زمینه تولید انواع خوراک دام،طیور و آبزیان مجموعه ای از بزرگتر",
    },
    {
      imageUrl: img.src,
      title: "اتحادیه، رونق بخش صنایع خوراک دام،طیور و آبزیان استان سمنان",
      description:
        "مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان برتر و تراز اول کشوردر زمینه تولید انواع خوراک دام،طیور و آبزیان مجموعه ای از بزرگتر",
    },
  ];

  return (
    <div className={style.container}>
      <div className="flex flex-row-reverse justify-start">
        <Button
          text="آخرین خبرها"
          onClick={() => console.log("click")}
          style={{ color: "var(--primaryGreen)" }}
          className="border-[1px] border-black m-1 hover:outline-slate-900 hover:border-transparent"
        />
        <Button
          text="اطلاعیه ها"
          onClick={() => console.log("click")}
          className="border-[1px] border-black m-1 hover:outline-slate-900 hover:border-transparent"
        />
        <Button
          text="خبرهای تصویری"
          onClick={() => console.log("click")}
          className="border-[1px] border-black m-1 hover:outline-slate-900 hover:border-transparen"
        />
      </div>
      <Carousel responsive={responsive} infinite arrows>
        {image.map((item, index) => (
          <BlogCard
            imageUrl={item.imageUrl}
            title={item.title}
            description={item.description}
            key={index}
            onClick={() => console.log("clicked")}
          />
        ))}
      </Carousel>
    </div>
  );
};

export default BlogSection;
