import Image from "next/image";
import IntroductionProps from "./../../../logic/inteface/Introduction";
import vet from "../../../assets/images/cow.jpg";
import styles from "./../../../styles/Introduction.module.css";
import { FC } from "react";
import ProductIntroduction from "./ProductIntroduction/ProductIntroduction";
const Introduction: FC<IntroductionProps> = ({}: IntroductionProps) => {
  return (
    <div className="flex flex-col md:flex-row justify-center items-center text-center my-12">
      <div className={`flex w-10/12 md:w-5/12 m-auto ${styles.leftBox}`}>
        <figure className="relative w-1/2 h-full inline-block pr-2">
          <Image src={vet} className="h-full" layout="fill" objectFit="cover" />
        </figure>
        <hr className={styles.verticalHr} />
        <div className="relative w-1/2 h-[400px] d-flex">
          <figure className="relative w-full h-1/2 flex-1">
            <Image src={vet} layout="fill" objectFit="cover" />
          </figure>
          <hr className={styles.horizontalHr} />
          <figure className="relative w-full h-1/2 flex-1">
            <Image src={vet} layout="fill" objectFit="cover" />
          </figure>
        </div>
      </div>
      <div className="flex w-10/12 md:w-5/12 mt-5 md:m-auto">
        <ProductIntroduction
          title="اتحادیه تعاونی های کارخانجات
خوراک دام، طیور و آبزیان استان سمنان"
          description="مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان برتر و
تراز اول کشوردر زمینه تولید انواع خوراک دام، طیور و
آبزیان مجموعه ای از بزرگترین ومجهزترین تولیدکنندگان
برتر و تراز اول کشوردر زمینه تولید انواع خوراک دام،
طیور و آبزیان"
        />
      </div>
    </div>
  );
};

export default Introduction;
