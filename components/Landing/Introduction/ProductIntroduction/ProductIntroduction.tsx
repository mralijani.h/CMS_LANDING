import { FC } from "react";
import Button from "../../../common/Button/Button";
import ProductIntroductionProps from "./../../../../logic/inteface/ProductIntroduction";
import styles from "../../../../styles/ProductIntroduction.module.css";
import RCSlider from "./../../../common/RCSlider/RCSlider";

const ProductIntroduction: FC<ProductIntroductionProps> = ({
  title,
  description,
}: ProductIntroductionProps) => {
  return (
    <div className="text-right">
      <h6 className={`text-2xl cursor-default ${styles.title}`}>{title}</h6>
      <RCSlider containerClassName="mb-6" />
      <p className="text-lg cursor-default ">{description}</p>
      <Button
        className="text-white"
        text="مشاهده محصولات"
        onClick={() => console.log("clicked")}
        color="var(--primaryGreen)"
      />
    </div>
  );
};

export default ProductIntroduction;
