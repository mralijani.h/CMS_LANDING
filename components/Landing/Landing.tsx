import type { NextPage } from "next";
import Slider from "../../components/Landing/Slider/Slider";
import Header from "./../../components/common/Header/Header";
import Menu from "./../../components/common/Menu/Menu";
import img from "../../assets/images/nature.jpg";
import img1 from "../../assets/images/statistics.jpg";
import ServiceCard from "./../../components/common/Cards/ServiceCard/ServiceCard";
import ServiceCardIcon from "./../../assets/images/serviceCard.png";
import Introduction from "./../../components/Landing/Introduction/Introduction";
import BlogSection from "../../components/Landing/BlogSection/BlogSection";
import ProductIntroductionSection from "./../../components/Landing/ProductIntroductionSection/ProductIntroductionSection";
import Statistics from "../../components/Landing/Statistics/Statistics";
import Footer from "./../../components/common/Footer/Footer";
// import "leaflet/dist/leaflet.css";
const Landing: NextPage = () => {
  const sliderImages = [img.src, img1.src, img.src];
  const serviceCard = [
    {
      text: "کارخانه های عضو",
      icon: ServiceCardIcon.src,
    },
    {
      text: " بورس نهاده",
      icon: ServiceCardIcon.src,
    },
    {
      text: "تخصیص نهاده",
      icon: ServiceCardIcon.src,
    },
    {
      text: "مشاوران اتحادیه",
      icon: ServiceCardIcon.src,
    },
  ];
  return (
    <div className="w-full m-0">
      <header>
        <Header />
        <Menu />
      </header>
      <main>
        <Slider image={sliderImages} />
        <div className="flex flex-row flex-wrap justify-evenly -mt-20 relative px-20">
          {serviceCard.map((service, index) => (
            <ServiceCard Text={service.text} Icon={service.icon} key={index} />
          ))}
        </div>
        <Introduction />
        <BlogSection />
        <ProductIntroductionSection />
        <Statistics />
      </main>
      <Footer />
    </div>
  );
};
export default Landing;
