import { FC } from "react";
import ProductIntroductionSectionProps from "../../../logic/inteface/ProductIntroductionSection";
import Carousel from "react-multi-carousel";
import ProductCard from "../../common/Cards/ProductCard/ProductCard";
const ProductIntroductionSection: FC<
  ProductIntroductionSectionProps
> = ({}: ProductIntroductionSectionProps) => {
  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 4,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 3,
    },
    tablet: {
      breakpoint: { max: 1024, min: 600 },
      items: 2,
    },
    mobile: {
      breakpoint: { max: 600, min: 0 },
      items: 1,
    },
  };

  const items = [
    {
      title: "قیمت خوراک دام سبک و فول فت سویا",
      prices: [
        "گوسفند و بز :000/39ر",
        "فول فت رست دانه کامل : 000/59 ر",
        "فول فت رست دانه کامل : 000/59 ر",
      ],
      info: "در سامانه بازارگاه 1400/11/2",
    },
    {
      title: "قیمت خوراک دام سبک و فول فت سویا",
      prices: [
        "گوسفند و بز :000/39ر",
        "فول فت رست دانه کامل : 000/59 ر",
        "فول فت رست دانه کامل : 000/59 ر",
      ],
      info: "در سامانه بازارگاه 1400/11/2",
    },
    {
      title: "قیمت خوراک دام سبک و فول فت سویا",
      prices: [
        "گوسفند و بز :000/39ر",
        "فول فت رست دانه کامل : 000/59 ر",
        "فول فت رست دانه کامل : 000/59 ر",
      ],
      info: "در سامانه بازارگاه 1400/11/2",
    },
    {
      title: "قیمت خوراک دام سبک و فول فت سویا",
      prices: [
        "گوسفند و بز :000/39ر",
        "فول فت رست دانه کامل : 000/59 ر",
        "فول فت رست دانه کامل : 000/59 ر",
      ],
      info: "در سامانه بازارگاه 1400/11/2",
    },
    {
      title: "قیمت خوراک دام سبک و فول فت سویا",
      prices: [
        "گوسفند و بز :000/39ر",
        "فول فت رست دانه کامل : 000/59 ر",
        "فول فت رست دانه کامل : 000/59 ر",
      ],
      info: "در سامانه بازارگاه 1400/11/2",
    },
    {
      title: "قیمت خوراک دام سبک و فول فت سویا",
      prices: [
        "گوسفند و بز :000/39ر",
        "فول فت رست دانه کامل : 000/59 ر",
        "فول فت رست دانه کامل : 000/59 ر",
      ],
      info: "در سامانه بازارگاه 1400/11/2",
    },
  ];
  return (
    <div className="mb-14">
      <h6 className="text-center text-2xl mt-16 mb-14">معرفی محصولات</h6>
      <div>
        <Carousel responsive={responsive} infinite>
          {items.map((item, index) => (
            <ProductCard
              info={item.info}
              prices={item.prices}
              title={item.title}
              key={index}
            />
          ))}
        </Carousel>
      </div>
    </div>
  );
};

export default ProductIntroductionSection;
