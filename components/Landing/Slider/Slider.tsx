import { FC } from "react";
import SliderProps from "./../../../logic/inteface/Slider";
import { FaArrowLeft, FaArrowRight } from "react-icons/fa";
import "react-slideshow-image/dist/styles.css";
import SliderItem from "./../../common/SliderItem/SliderItem";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import styles from "../../../styles/Slider.module.css";
import "react-multi-carousel/lib/styles.css";
import Carousel from "react-multi-carousel";

const Slider: FC<SliderProps> = ({ image }: SliderProps) => {
  const responsive = {
    superLargeDesktop: {
      breakpoint: { max: 4000, min: 3000 },
      items: 1,
    },
    desktop: {
      breakpoint: { max: 3000, min: 1024 },
      items: 1,
    },
    tablet: {
      breakpoint: { max: 1024, min: 464 },
      items: 1,
    },
    mobile: {
      breakpoint: { max: 464, min: 0 },
      items: 1,
    },
  };
  const CustomLeftArrow = ({ onClick, ...rest }: any) => {
    return (
      <div
        className="absolute left-4 top-1/2 -translate-y-1/2
      text-white rounded-full border-2 p-2 cursor-pointer z-9"
        onClick={() => onClick()}
      >
        <FaArrowLeft size={20} />
      </div>
    );
  };
  const CustomRightArrow = ({ onClick, ...rest }: any) => {
    return (
      <div
        className="absolute right-4 top-1/2 -translate-y-1/2
      text-white rounded-full border-2 p-2 cursor-pointer z-9"
        onClick={() => onClick()}
      >
        <FaArrowRight size={20} />
      </div>
    );
  };

  return (
    <div
      className={`w-full border-2 rounded overflow-hidden ` + styles.slideShow}
    >
      <div className="w-full h-full">
        <Carousel
          autoPlay
          autoPlaySpeed={2000}
          infinite
          responsive={responsive}
          arrows
          slidesToSlide={1}
          className="h-full items-stretch left-0"
          itemClass="w-full"
          sliderClass="h-full"
          customRightArrow={<CustomRightArrow />}
          customLeftArrow={<CustomLeftArrow />}
        >
          {image.map((image, index) => (
            <SliderItem
              key={index}
              src={image}
              h4Text="اتحادیه، رونق بخش صنایع خوراک
دام،طیور و آبزیان"
              pText="مجموعه ای از بزرگترین ومجهزترین
تولیدکنندگان برتر و تراز اول کشوردر زمینه
تولید انواع خوراک دام، طیور و آبزیان"
            />
          ))}
        </Carousel>
      </div>
    </div>
  );
};

export default Slider;
