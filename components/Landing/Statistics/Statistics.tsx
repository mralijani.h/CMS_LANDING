import { FC } from "react";
import StatisticsProps from "./../../../logic/inteface/Statistics";
import styles from "../../../styles/Statistics.module.css";
import icon from "../../../assets/images/productIcon.png";
import StatisticsItem from "./StatisticsItem/StatisticsItem";
const Statistics: FC<StatisticsProps> = ({}: StatisticsProps) => {
  const statisticsData = [
    {
      imageUrl: icon.src,
      data: "152,000 MT",
      title: "ظرفیت اسمی خوراک آبزیان",
    },
    {
      imageUrl: icon.src,
      data: "502,000 MT",
      title: "تعداد کارخانجات عضو",
    },
    {
      imageUrl: icon.src,
      data: "152,000 MT",
      title: "ظرفیت اسمی خوراک آبزیان",
    },
    {
      imageUrl: icon.src,
      data: "12,000 MT",
      title: "تعداد کارخانجات عضو",
    },
  ];
  return (
    <div className={styles.container}>
      <div className="bg-slate-700 bg-opacity-50 flex flex-row flex-wrap justify-around">
        {statisticsData.map((item, index) => (
          <StatisticsItem
            imageUrl={item.imageUrl}
            title={item.title}
            data={item.data}
            key={index}
          />
        ))}
      </div>
    </div>
  );
};

export default Statistics;
