import { FC } from "react";
import StatisticsItemProps from "./../../../../logic/inteface/StatisticsItem";
import Image from "next/image";
const StatisticsItem: FC<StatisticsItemProps> = ({
  data,
  imageUrl,
  title,
}: StatisticsItemProps) => {
  return (
    <div className="text-center text-white py-24">
      <Image
        src={imageUrl}
        width="105%"
        height="90%"
        objectFit="cover"
        className="block"
      />

      <span className="block text-4xl">{data}</span>
      <h4 className="text-xl">{title}</h4>
    </div>
  );
};
export default StatisticsItem;
