import { FC } from "react";
import ButtonProps from "../../../logic/inteface/Button";
const Button: FC<ButtonProps> = (props: ButtonProps) => {
  return (
    <button
      className={`py-2 px-4 rounded-xl block cursor-pointer hover:outline-2 hover:outline-double ${
        props.className ? props.className : ""
      }`}
      style={{
        backgroundColor: props.color ? props.color : undefined,
        ...props.style,
      }}
      onClick={props.onClick ? props.onClick : () => null}
    >
      {props.text}
    </button>
  );
};
export default Button;
