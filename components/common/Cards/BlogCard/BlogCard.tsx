import { FC } from "react";
import BlogCardProps from "./../../../../logic/inteface/BlogCard";
import styles from "../../../../styles/Blogcard.module.css";
import Image from "next/image";
import Button from "./../../Button/Button";
const BlogCard: FC<BlogCardProps> = ({
  title,
  description,
  onClick,
  imageUrl,
}: BlogCardProps) => {
  return (
    <div className="rounded-t-md w-10/12 mx-auto my-2 text-right">
      <figure>
        <Image
          src={imageUrl}
          width="60%"
          height="30%"
          layout="responsive"
          objectFit="cover"
          className="rounded-t-md"
        />
      </figure>
      <div className="p-4 bg-gray-50">
        <h4 className="text-xl mb-4">{title}</h4>
        <p>{description}</p>
        <Button
          text="ادامه مطلب"
          onClick={() => onClick && onClick()}
          className={
            "text-white border-2  border-transparent hover:border-2 hover:border-black " +
            styles.button
          }
        />
      </div>
    </div>
  );
};

export default BlogCard;
