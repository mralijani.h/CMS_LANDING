import { FC } from "react";
import styles from "../../../../styles/ProductCard.module.css";
import ProductCardProps from "./../../../../logic/inteface/ProductCard";
const ProductCard: FC<ProductCardProps> = ({
  info,
  prices,
  title,
}: ProductCardProps) => {
  return (
    <div
      className={
        "rounded-2xl w-11/12 mx-auto my-2 text-right relative " +
        styles.container
      }
    >
      <div
        className={
          "px-8 pt-12 pb-20 text-center text-white rounded-t-2xl " +
          styles.innerContainer
        }
      >
        {prices.map((price, index) => (
          <div
            className="text-sm border-b-2 border-orange-500 w-fit mx-auto px-6 py-2"
            key={index}
          >
            {price}
          </div>
        ))}
      </div>
      <div
        className={
          "w-full rounded-b-2xl px-8 py-4 text-white text-center " +
          styles.bottomBox
        }
      >
        <div>{title}</div>
        <div className="text-orange-300">{info}</div>
      </div>
    </div>
  );
};

export default ProductCard;
