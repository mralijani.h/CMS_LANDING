import { FC } from "react";
import ServiceCardProps from "./../../../../logic/inteface/ServiceCard";
import Image from "next/image";
import styles from "../../../../styles/ServiceCard.module.css";
const ServiceCard: FC<ServiceCardProps> = ({
  Icon,
  Text,
}: ServiceCardProps) => {
  return (
    <div
      className={`rounded-md cursor-default bg-white ${styles.serviceContainer}`}
    >
      <div className="flex flex-col justify-center items-center h-full rounded-md text-center relative -top-0.5">
        <i className="w-full block ">
          {typeof Icon == "string" ? (
            <Image src={Icon} width={"50%"} height={"50%"} />
          ) : typeof Icon == "function" ? (
            <Icon size={"50%"} />
          ) : null}
        </i>
        <h6 className="text-lg">{Text}</h6>
      </div>
    </div>
  );
};

export default ServiceCard;
