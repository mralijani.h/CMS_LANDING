import { FC } from "react";
import FooterProps from "./../../../logic/inteface/Footer";
import styles from "./../../../styles/Footer.module.css";
import Link from "next/link";
import {
  FaPhone,
  FaRegEnvelopeOpen,
  FaRegEnvelope,
  FaFax,
  FaTwitter,
  FaFacebookF,
  FaInstagram,
  FaMapPin,
} from "react-icons/fa";
import dynamic from "next/dynamic";

const Footer: FC<FooterProps> = ({}: FooterProps) => {
  // const Map = dynamic(() => import("../Map/Map"), {
  //   ssr: false,
  // });
  return (
    <footer>
      <div
        className={
          styles.container +
          " py-10 pl-10 pr-16 flex flex-col-reverse lg:flex-row"
        }
      >
        <div className="flex-[1] flex flex-row p-3">
          <div
            className={
              "my-auto flex flex-col justify-center items-center " +
              styles.primaryColor
            }
          >
            <Link href="#">
              <a className="my-2">
                <FaFacebookF size={20} />
              </a>
            </Link>
            <Link href="#">
              <a className="my-4 rounded-full bg-slate-300 max-w-fit p-2">
                <FaTwitter size={20} />
              </a>
            </Link>
            <Link href="#">
              <a className="my-2">
                <FaInstagram size={20} />
              </a>
            </Link>
          </div>
          <div className="flex-[12] text-right">
            <p className="inline-block w-11/12 text-right mb-2">
              سمنان، خیابان هاشمی ، بالاتر از پارک لاله
            </p>
            <FaMapPin
              className={
                styles.primaryColor + " inline-block align-top mr-auto"
              }
              size={25}
            />
            <div className="rounded-lg overflow-hidden ml-3">
              {/* <Map
                long={35.719534}
                lat={53.365575}
                mapHeight={250}
                className="border-2 border-slate-500"
              /> */}
            </div>
          </div>
        </div>
        <div className="flex flex-row justify-around flex-[1] text-right">
          <div className="ml-10 md:ml-0">
            <strong className="mb-4 text-2xl inline-block">ارتباط با ما</strong>
            <ul className="">
              <li className="my-2">
                <span className="tracking-wider align-sub">011-33119999</span>
                <FaPhone
                  className={"inline-block ml-2 " + styles.primaryColor}
                  size={20}
                />
              </li>
              <li className="my-2">
                <span className="tracking-wider align-sub">info@sbsd.com</span>
                <FaRegEnvelope
                  className={"inline-block ml-2 " + styles.primaryColor}
                  size={20}
                />
              </li>
              <li className="my-2">
                <span className="tracking-wider align-sub">209886537633</span>
                <FaRegEnvelopeOpen
                  className={"inline-block ml-2 " + styles.primaryColor}
                  size={20}
                />
              </li>
              <li className="my-2">
                <span className="tracking-wider align-sub">209886537633</span>
                <FaFax
                  className={"inline-block ml-2 " + styles.primaryColor}
                  size={20}
                />
              </li>
            </ul>
          </div>
          <div className="text-right ml-auto ">
            <strong className="text-2xl mb-4 block">لینک ها</strong>
            <div className="flex flex-col md:flex-row-reverse">
              <div>
                <ul>
                  <li className="my-2">
                    <Link href="#">صفحه اصلی</Link>
                  </li>
                  <li className="my-2">
                    <Link href="#">اتحادیه ما</Link>
                  </li>
                  <li className="my-2">
                    <Link href="#">اولویت ها</Link>
                  </li>
                  <li className="my-2">
                    <Link href="#">آموزش و منابع</Link>
                  </li>
                  <li className="my-2">
                    <Link href="#">تضمین کیفیت</Link>
                  </li>
                </ul>
              </div>

              <div className="md:mr-10 ">
                <ul>
                  <li className="my-2">
                    <Link href="#">شفافیت و اعتماد سازی</Link>
                  </li>
                  <li className="my-2">
                    <Link href="#">اتاق خبر</Link>
                  </li>
                  <li className="my-2">
                    <Link href="#">اعضا</Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="bg-white text-center py-2">
        .کلیه حقوق مادی و معنوی متعلق به اتحادیه تعاونی های کارخانجات دام، طیور
        و آبزیان می باشد
      </div>
    </footer>
  );
};

export default Footer;
