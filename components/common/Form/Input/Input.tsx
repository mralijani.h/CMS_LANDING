import InputProps from "../../../../logic/inteface/Input";
import { FC } from "react";

const Input: FC<InputProps> = (props: InputProps) => {
  return (
    <div className="d-flex justify-content-between align-items-center w-full">
      {props.Icon && <props.Icon size={25} className="m-2" role="button" />}
      <input
        className="form-control block m-0 w-full border-2 rounded h-8"
        type={props.type ? props.type : "text"}
        placeholder={props.placeholder}
      />
    </div>
  );
};
export default Input;
