import { FC } from "react";
import Link from "next/link";
import HeaderProps from "./../../../logic/inteface/Header";
import styles from "../../../styles/Header.module.css";
import HeaderItem from "./HeaderItem/HeaderItem";
import { FaCalendar, FaClock, FaDoorOpen } from "react-icons/fa";
import {
  getCurrentDate,
  getCurrentTime,
} from "../../../logic/utils/CurrentDateTime";
const Header: FC<HeaderProps> = ({}: HeaderProps) => {
  return (
    <>
      <nav className={styles.container + " px-2"}>
        <div
          className={`md:justify-between md:flex-row md:flex md:w-full md:flex-wrap sm:justify-center`}
        >
          <div className={`text-white flex justify-around items-center`}>
            <span className="flex-2 sm:flex sm:justify-center md:block">
              <HeaderItem Icon={FaDoorOpen} Text="ورود" />
            </span>
            <hr className={styles.hr} />
            <span className="flex-2 sm:flex sm:justify-center">
              <HeaderItem Icon={FaClock} Text={getCurrentTime()} />
            </span>

            <hr className={styles.hr} />
            <span className="flex-3 sm:flex sm:justify-center">
              <HeaderItem Icon={FaCalendar} Text={getCurrentDate()} />
            </span>
          </div>
          <div className="items-center flex text-white flex-row-reverse font-semibold">
            <Link href="#">
              <span className={`text-white mx-2 ${styles.link}`}>اخبار</span>
            </Link>
            <Link href="#">
              <span className={`text-white mx-2 ${styles.link}`}>اطلاعیه</span>
            </Link>
            <Link href="#">
              <span className={`text-white mx-2 ${styles.link}`}>
                خبر تصویری
              </span>
            </Link>
            <Link href="#">
              <span className={`text-white mx-2 ${styles.link}`}>
                ارتباط با ما
              </span>
            </Link>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Header;
