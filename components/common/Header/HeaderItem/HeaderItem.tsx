import { FC } from "react";
import HeaderItemProps from "../../../../logic/inteface/HeaderItem";
const HeaderItem: FC<HeaderItemProps> = ({
  Text,
  Icon,
  className,
}: HeaderItemProps) => {
  return (
    <div
      className={`flex flex-row flex-wrap items-center mx-2 align-middle h-[45px] ${className}`}
    >
      <span className="mx-1">{Text}</span>
      <Icon />
    </div>
  );
};

export default HeaderItem;
