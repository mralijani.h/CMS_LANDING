import { FC } from "react";
import MapProps from "./../../../logic/inteface/Map";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import "leaflet-defaulticon-compatibility/dist/leaflet-defaulticon-compatibility.webpack.css"; // Re-uses images from ~leaflet package
import "leaflet-defaulticon-compatibility";

const Map: FC<MapProps> = ({ long, lat, mapHeight, className }: MapProps) => {
  return (
    <MapContainer
      center={[long, lat]}
      zoom={16}
      scrollWheelZoom
      className={"w-full h-full " + className}
      style={{ height: mapHeight ? mapHeight : "250px" }}
      dragging
    >
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      <Marker position={[long, lat]}>
        <Popup>شرکت فلان</Popup>
      </Marker>
    </MapContainer>
  );
};

export default Map;
