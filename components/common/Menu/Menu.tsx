import { useState, FC } from "react";
import MenuProps from "./../../../logic/inteface/Menu";
import { FaSearch, FaBars } from "react-icons/fa";
import CustomModal from "./../Modal/Modal";
import Input from "./../../common/Form/Input/Input";
import MenuItem from "./MenuItem/MenuItem";

const Menu: FC<MenuProps> = ({}: MenuProps) => {
  const [show, setShow] = useState<boolean>(false);
  const [showMenu, setShowMenu] = useState<boolean>(true);
  function MenuToggle() {
    const ul = document.getElementById("menuUL");

    if (ul) {
      if (showMenu === true) {
        ul.classList.add("top-[70px]");
        ul.classList.add("opacity-100");
        ul.classList.remove("opacity-0");
        ul.classList.remove("top-[-400px]");
        ul.classList.remove("invisible");
      }
      if (showMenu === false) {
        ul.classList.remove("top-[70px]");
        ul.classList.remove("opacity-100");
        ul.classList.add("opacity-0");
        ul.classList.add("top-[-400px]");
        ul.classList.add("invisible");
      }
    }
    setShowMenu(!showMenu);
  }
  return (
    <>
      <nav className="relative p-5 shadow md:flex md:items-center md:justify-between flex-row-reverse flex-nowrap ">
        <div className="flex justify-between items-center self-start md:w-full md:justify-end lg:w-auto">
          <span className="text-2xl cursor-pointer md:mx-auto">LoGO</span>
          <span
            className="text-3xl cursor-pointer mx-2 lg:hidden block z-[2]"
            onClick={MenuToggle}
          >
            <FaBars />
          </span>
        </div>

        <ul
          className="sm:flex sm:flex-col text-right sm:items-end md:flex lg:items-start lg:static absolute bg-white w-full right-0 md:w-auto md:py-0 py-4 md:pl-0 pr-4 lg:opacity-100 top-[-400px] lg:flex-row-reverse 
          lg:ml-auto lg:align-top
           transition-all ease-in duration-500 shadow-inner lg:shadow-none opacity-0 z-[8] invisible md:visible"
          id="menuUL"
        >
          <li className="mx-2 my-6 sm:my-2 sm:mt-4 lg:mt-0 ">
            <MenuItem text="صفحه اصلی" href="#" />
          </li>
          <li className="mx-2 my-6 sm:my-2 lg:mt-0">
            <MenuItem text="اتحادیه ما" href="#" />
          </li>
          <li className="mx-2 my-6 sm:my-2 lg:mt-0">
            <MenuItem text="اولویت ها" href="#" />
          </li>
          <li className="mx-2 my-6 sm:my-2 lg:mt-0">
            <MenuItem text="آموزش و منابع" href="#" />
          </li>
          <li className="mx-2 my-6 sm:my-2 lg:mt-0">
            <MenuItem text="تضمین کیفیت" href="#" />
          </li>
          <li className="mx-2 my-6 sm:my-2 lg:mt-0">
            <MenuItem text="شفافیت و اعتماد سازی" href="#" />
          </li>
          <li className="mx-2 my-6 sm:my-2 lg:mt-0">
            <MenuItem text="اتاق خبر" href="#" />
          </li>
          <li className="mx-2 my-6 sm:my-2 lg:mt-0">
            <MenuItem text="سامانه های مرتبط" href="#" />
          </li>
          <li className="mx-2 my-6 sm:my-2 lg:mt-0">
            <MenuItem text="اعضا" href="#" />
          </li>
        </ul>
        <span
          className="text-3xl cursor-pointer mx-2 block mt-[-30px] md:mt-0"
          onClick={() => setShow(!show)}
        >
          <FaSearch className="mx-auto relative" />
          <span className="block md:hidden text-sm text-center">جستجو</span>
        </span>
        <CustomModal
          actionBtnText="جستجو"
          modalTitle="جستجو در سایت"
          modalBodyChild={<Input placeholder="...جستجو" />}
          actionFunction={() => console.log("clicked")}
          toggleFunction={(value) => setShow(value)}
          show={show}
        />
      </nav>
    </>
  );
};

export default Menu;
