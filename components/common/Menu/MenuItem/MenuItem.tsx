import { FC } from "react";
import MenuItemProps from "../../../../logic/inteface/MenuItem";
import Link from "next/link";
import styles from "../../../../styles/MenuItem.module.css";
const MenuItem: FC<MenuItemProps> = ({ href, text }: MenuItemProps) => {
  return (
    <Link href={href}>
      <span className={styles.menuLink + " sm:text-sm xl:text-lg pb-1"}>
        {text}
      </span>
    </Link>
  );
};

export default MenuItem;
