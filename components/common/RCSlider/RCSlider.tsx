import { FC } from "react";
import RCSliderProps from "./../../../logic/inteface/RCSlider";
import styles from "../../../styles/RCSlider.module.css";
const RCSlider: FC<RCSliderProps> = ({
  containerClassName,
  dotColor,
  sliderColor,
}: RCSliderProps) => {
  return (
    <div className={containerClassName + " ml-auto " + styles.container}>
      <div className={styles.slider} style={{ backgroundColor: sliderColor }}>
        <span
          className={styles.dot}
          style={{ backgroundColor: dotColor }}
        ></span>
      </div>
    </div>
  );
};

export default RCSlider;
