import { FC } from "react";
import Image from "next/image";
import SliderItemProps from "./../../../logic/inteface/SliderItem";
import Button from "../Button/Button";

const SliderItem: FC<SliderItemProps> = ({
  h4Text,
  pText,
  src,
}: SliderItemProps) => {
  return (
    <div className="h-full overflow-hidden relative text-white">
      <Image src={src} layout="fill" objectFit="fill" />
      <div className="absolute top-1/4 md:top-1/3 -translate-y-1/3 lg:-translate-y-1/2 right-20 md:right-20 xl:right-40 w-full md:w-10/12 text-right">
        <h4 className="border-r-4 border-orange-500 pr-2 w-1/2 ml-auto md:text-2xl lg:text-3xl cursor-default">
          {h4Text}
        </h4>
        <p className="mt-4 md:text-xl lg:text-2lx w-2/3 md:w-1/2 ml-auto cursor-default">
          {pText}
        </p>
        <Button
          text="ادامه مطلب"
          className="bg-orange-500 mx-auto"
          onClick={() => console.log("clicked")}
        />
      </div>
    </div>
  );
};

export default SliderItem;
