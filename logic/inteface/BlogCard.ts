export default interface BlogCardProps {
  title: string;
  description: string;
  imageUrl: string;
  onClick?: () => any;
}
