import { CSSProperties } from "react";

export default interface ButtonProps {
  text: string;
  color?: string;
  onClick: () => any;
  className?: string;
  style?: CSSProperties;
}
