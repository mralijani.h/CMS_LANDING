import { IconType } from "react-icons";
export default interface HeaderItemProps {
  Text: string;
  Icon: IconType;
  className?: string;
}
