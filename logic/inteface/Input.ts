import { IconType } from "react-icons";
export default interface InputProps {
  placeholder?: string;
  Icon?: IconType;
  type?: string;
}
