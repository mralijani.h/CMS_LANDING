export default interface MapProps {
  long: number; // toole joqrafiaii
  lat: number; // arze joqrafiaii
  mapHeight?: number;
  className?: string;
}
