export default interface MenuItemProps {
  href: string;
  text: string;
}
