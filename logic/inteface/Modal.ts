export default interface ModalProps {
  modalTitle: string;
  modalBodyChild: any;
  closeBtnText?: string;
  actionBtnText?: string;
  actionFunction: () => any;
  toggleFunction: (arg0: boolean) => any;
  show: boolean;
}
