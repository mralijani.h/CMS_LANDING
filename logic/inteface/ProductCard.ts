export default interface ProductCardProps {
  title: string;
  prices: string[];
  info: string;
}
