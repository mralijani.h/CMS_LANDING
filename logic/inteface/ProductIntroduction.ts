export default interface ProductIntroductionProps {
  title: string;
  description: string;
}
