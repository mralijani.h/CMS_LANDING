export default interface RCSliderProps {
  containerClassName?: string;
  dotColor?: string;
  sliderColor?: string;
}
