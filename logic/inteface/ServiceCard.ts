import { IconType } from "react-icons";
export default interface ServiceCardProps {
  Text: string;
  Icon?: IconType | string;
}
