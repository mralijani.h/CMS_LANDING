export default interface SliderItemSMProps {
  title: string;
  description: string;
  imageUrl: string;
  onClick?: () => any;
}
