export default interface StatisticsItemProps {
  imageUrl: string;
  data: string;
  title: string;
}
