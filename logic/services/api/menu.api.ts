import { useMutation } from "react-query";
import methods from "../interceptor/http.interceptor";
import { IAxiosResult } from "../../models/axios-result.model";
import { AxiosResponse } from "axios";

const MainUrl = process.env.REACT_APP_PUBLIC_PATH;

export interface ILogin {
  emailOrUsername: string;
  password: string;
}

export interface IRegister {
  nationalCode: string;
  cellphone: string;
  // password: string;
}

const GetMenu = async (value: ILogin): Promise<AxiosResponse<IAxiosResult>> => {
  return await methods.post(MainUrl + "/api/Authentication/Login", value);
};

const Register = async (
  value: IRegister
): Promise<AxiosResponse<IAxiosResult>> => {
  return await methods.post(MainUrl + "/api/Account/AddPotentialUser", value);
};

export const useMenu = () => {
  return useMutation(GetMenu, {});
};
