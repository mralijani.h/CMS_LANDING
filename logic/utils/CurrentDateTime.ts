import moment from "jalali-moment";

const getCurrentDate = (): string => {
  const momentObj = moment();
  const dayInWeek = momentObj.locale("fa").jDay();
  let dayInWeekString;
  switch (dayInWeek) {
    case 0:
      dayInWeekString = "شنبه";
      break;
    case 1:
      dayInWeekString = "یکشنبه";
      break;
    case 2:
      dayInWeekString = "دوشنبه";
      break;
    case 3:
      dayInWeekString = "سه شنبه";
      break;
    case 4:
      dayInWeekString = "چهار شنبه";
      break;
    case 5:
      dayInWeekString = "پنج شنبه";
      break;
    case 6:
      dayInWeekString = "جمعه";
      break;
  }
  const dayInMonth = momentObj.jDate().toLocaleString("fa-IR");
  const month = momentObj.add(1, "M").jMonth();
  let monthInString;
  switch (month) {
    case 1:
      monthInString = "فروردین";
      break;

    case 2:
      monthInString = "اردیبهشت";
      break;

    case 3:
      monthInString = "خرداد";
      break;

    case 4:
      monthInString = "تیر";
      break;

    case 5:
      monthInString = "مرداد";
      break;

    case 6:
      monthInString = "شهریور";
      break;
    case 7:
      monthInString = "مهر";
      break;

    case 8:
      monthInString = "آبان";
      break;
    case 9:
      monthInString = "آذر";
      break;
    case 10:
      monthInString = "دی";
      break;
    case 11:
      monthInString = "بهمن";
      break;
    case 12:
      monthInString = "اسفند";
      break;
  }
  const date = `${dayInWeekString} ${dayInMonth} ${monthInString}`;
  return date;
};

const getCurrentTime = (): string => {
  const momentObj = moment();
  const hours = momentObj.hours().toLocaleString("fa");
  const minutes = momentObj.minutes();
  let twoDigitMinute;
  if (minutes < 10) twoDigitMinute = "۰" + minutes.toLocaleString("fa");
  else twoDigitMinute = minutes.toLocaleString("fa");
  return `${hours}:${twoDigitMinute}`;
};

export { getCurrentDate, getCurrentTime };
